# Classification Using Machine Learning

This project is for a UG Course of Machine Learning (5th Semester), PES University.

## Problem Statement

Explore the e cacy of machine learning (ML) in characterizing exoplanets into di erent classes. The source of
the data used in this work is University of Puerto Rico's Planetary Habitability Laboratory's Exoplanets Catalog
(PHL-EC). Perform a detailed analysis of the structure of the data and propose methods that can be used to
e ectively categorize new exoplanet samples. Contributions are two-fold; elaborate on the results obtained
by using ML algorithms by stating the accuracy of each method used and propose a paradigm to automate the task
of exoplanet classification for relevant outcomes. 

## How to Use the Environment (Anaconda only)

If you have anaconda, you can make use of the [mlpesu.yml](/mlpesu.yml) file to create a new environment. Use this command in your Anaconda (PowerShell) Prompt:
```shell
conda env create -f .\mlpesu.yml
conda activate mlpesu
```

## To run the notebook

1. `cd` into the current working directoy which contains the `.ipynb` file.
2. Run this command from Anaconda (PowerShell) Prompt:
    ```shell
	   jupyter notebook
	```

## Contributions

1. [Hrishikesh V](https://www.linkedin.com/in/hrishivish23/)
2. [Ravendra Singh](https://www.linkedin.com/in/ravendra-singh-729962bb/)
3. [Varun R Gupta](https://www.linkedin.com/in/varungupta3009/)
4. [Aekansh Dixit](https://www.linkedin.com/in/aekanshdixit/)